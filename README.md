# LSZ1.0_QGIS2.18
LSZ 1.0
begin : 2018-09-24/
copyright : (C) 2018 CNR-IRPI/
email : giacomo.titti@irpi.cnr.it
 
/******************************************************************************

    LSZ 1.0                                          
    Copyright (C) 2018 by CNR-IRPI            

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/


The LSZ (Landslide Susceptibility Zoning) tool has been developed for QGIS 2.x. The plugin has been developed to solve the landslide susceptibility assessment by the application of the Weights of Evidence (WoE) technique. The WoE technique
is a bivariate statistical approach introduced by Agterberg et al. (Agterberg et al., 1989) and then elaborated by Bonham-Carter et al. (Bonham-Carter et al., 1988).
The LSZ-plugin is a Python-based tool. It requires different libraries to be used such as: Numpy, PyQt, GDAL and Scikit-learn (Pedregosa et al., 2011).



