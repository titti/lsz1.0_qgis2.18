"""
/***************************************************************************
Soil parameters selection 1.0
        begin                : 2018-09-24
        copyright            : (C) 2018 by Giacomo Titti and Giulia Bossi, CNR-IRPI
        email                : giacomo.titti@irpi.cnr.it
 ***************************************************************************/

/***************************************************************************
    Soil parameters selection 1.0                                          
    Copyright (C) 2018 by Giacomo Titti and Giulia Bossi, CNR-IRPI            

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ***************************************************************************/
"""
#coding=utf-8

import os

from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'WoEmodule_dialog_base.ui'))


class WoEDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(WoEDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
