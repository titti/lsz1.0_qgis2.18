# coding: utf-8
"""
/***************************************************************************
LSZ 1.0
        begin                : 2018-09-24
        copyright            : (C) 2018 CNR-IRPI
        email                : giacomo.titti@irpi.cnr.it
 ***************************************************************************/

/***************************************************************************
    LSZ 1.0                                          
    Copyright (C) 2018 by CNR-IRPI            

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ***************************************************************************/
"""
#coding=utf-8
import numpy as np
from osgeo import gdal,osr
import sys
import math
import csv
from qgis.core import QgsMessageLog
import processing
import os
#import scipy.misc as im
import ogr

class model:
    def iter(self):########################control all functions
        listcause=[]
        listclasses=[]
        if self.Wcause1==None:
            pass
        else:
            listcause.append(self.Wcause1)
            listclasses.append(self.classes1)
        if self.Wcause2==None:
            pass
        else:
            listcause.append(self.Wcause2)
            listclasses.append(self.classes2)
        if self.Wcause3==None:
            pass
        else:
            listcause.append(self.Wcause3)
            listclasses.append(self.classes3)
        if self.Wcause4==None:
            pass
        else:
            listcause.append(self.Wcause4)
            listclasses.append(self.classes4)
        #########################################################################
        countcause=len(listcause)######delete empty cause!!!!!!!!!!
        if countcause==0:#verify empty row input
            QgsMessageLog.logMessage('Select at least one cause', tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # Select at least one cause, see 'WoE' Log Messages Panel
        #self.Wfs={}
        ####################################translate dem and inventory
        self.newXNumPxl=np.round(abs(self.xmax-self.xmin)/(self.w)).astype(int)
        self.newYNumPxl=np.round(abs(self.ymax-self.ymin)/(self.h)).astype(int)
        extent = "%s,%s,%s,%s" % (self.xmin, self.xmax, self.ymin, self.ymax)
        ##############
        ds0 = gdal.Open(self.Wdem)
        self.epsg = int(osr.SpatialReference(wkt=ds0.GetProjection()).GetAttrValue('AUTHORITY',1))
        del ds0
        if self.polynum==1:
            try:
                os.system('gdalwarp -ot Float32 -q -of GTiff -t_srs EPSG:'+str(self.epsg)+' -r bilinear -overwrite '+ self.Wdem+' /tmp/demreproj.tif')
                os.system('gdal_translate -of GTiff -ot Float32 -strict -outsize ' + str(self.newXNumPxl) +' '+ str(self.newYNumPxl) +' -projwin ' +str(self.xmin)+' '+str(self.ymax)+' '+ str(self.xmax) + ' ' + str(self.ymin) + ' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 /tmp/demreproj.tif /tmp/demq.tif')
            except:
                #print("Failure to set nodata values on raster dem")
                QgsMessageLog.logMessage("Failure to save sized /tmp dem", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Failure to save sized DEM, see 'WoE' Log Messages Panel
            try:
                ##os.system('ogr2ogr -t_srs EPSG:'+str(self.epsg)+' /tmp/poly.shp '+self.poly)
                
                #processing.runalg('saga:clipgridwithpolygon', '/tmp/sizedemxxx.tif',self.poly,1,'/tmp/demxxx.tif')
                #os.system('gdalwarp -ot Float32 -q -of GTiff -t_srs EPSG:'+str(self.epsg)+' -r bilinear -cutline /tmp/poly.shp -co GEOTIFF_KEYS_FLAVOR=ESRI_PE -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -overwrite -wo OPTIMIZE_SIZE=TRUE '+self.Wdem+' /tmp/demreproj.tif')
                #processing.runalg('grass7:r.mask.vect', self.poly,'/tmp/demq.tif',None,None,False,extent,0.0,-1.0,0.0001,'/tmp/demxxx.tif')
                os.system('gdalwarp -ot Float32 -q -of GTiff -dstnodata -9999 -cutline '+self.poly+' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE -overwrite /tmp/demq.tif /tmp/demxxx.tif')
                #processing.runalg('gdalogr:cliprasterbymasklayer', '/tmp/demq.tif',self.poly,None,False,False,False,5,4,75.0,6.0,1.0,False,0,False,None,'/tmp/demxxx.tif')
            except:
                #print("Failure to set nodata values on raster dem")
                QgsMessageLog.logMessage("Failure to save clipped dem", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Failure to save sized DEM, see 'WoE' Log Messages Panel
            print('ciao')
        else:
            try:
                os.system('gdalwarp -ot Float32 -q -of GTiff -t_srs EPSG:'+str(self.epsg)+' -r bilinear -overwrite '+ self.Wdem+' /tmp/demreproj.tif')
                os.system('gdal_translate -of GTiff -ot Float32 -strict -outsize ' + str(self.newXNumPxl) +' '+ str(self.newYNumPxl) +' -projwin ' +str(self.xmin)+' '+str(self.ymax)+' '+ str(self.xmax) + ' ' + str(self.ymin) + ' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 /tmp/demreproj.tif /tmp/demxxx.tif')
            except:
                #print("Failure to set nodata values on raster dem")
                QgsMessageLog.logMessage("Failure to save sized dem", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Failure to save sized DEM, see 'WoE' Log Messages Panel
            #########################save dem -9999
        try:
            self.ds=None
            self.ds = gdal.Open('/tmp/demxxx.tif')
            if self.ds is None:
                QgsMessageLog.logMessage("ERROR: can't open resized dem", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # can't open resized dem, see 'WoE' Log Messages Panel
            self.xsize = self.ds.RasterXSize
            self.ysize = self.ds.RasterYSize
            gtdem= self.ds.GetGeoTransform()
            NoDatadem=self.ds.GetRasterBand(1).GetNoDataValue()
            #########################################nodata -9999
            self.dem = np.array(self.ds.GetRasterBand(1).ReadAsArray()).astype('float32')
            self.dem[self.dem==NoDatadem]= -9999
            self.dem[np.isnan(self.dem)]= -9999
            ###########################################save dem
            driverdem = self.ds.GetDriver()
            out_datadem = driverdem.Create(self.fold + '/demnxn.tif', self.xsize, self.ysize, 1, gdal.GDT_Float32)
            dem_datadem = self.dem.astype('float32')
            out_banddem = out_datadem.GetRasterBand(1)
            out_banddem.WriteArray(dem_datadem, 0, 0)
            out_banddem.FlushCache()
            out_banddem.SetNoDataValue(-9999)
            out_datadem.SetGeoTransform(self.ds.GetGeoTransform())
            out_datadem.SetProjection(self.ds.GetProjection())
            #os.system('gdal_translate -of GTiff -ot Float32 -strict -outsize ' + str(newXNumPxl) +' '+ str(newYNumPxl) +' -a_nodata -9999 -projwin ' +str(self.xmin)+' '+str(self.ymax)+' '+ str(self.xmax) + ' ' + str(self.ymin) + ' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 ' + '/tmp/dem.tif' +' '+ self.fold + '/demnxn.tif')
        except:
            QgsMessageLog.logMessage("Failure to save sized dem", tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # Failure to save sized dem, see 'WoE' Log Messages Panel
        #finally:
            #dem_datadem=np.array([])
        ######################################load dem

        #######################################inventory from shp to tif
        try:
            driverd = ogr.GetDriverByName('ESRI Shapefile')
            ds9 = driverd.Open(self.inventory,0)
            layer = ds9.GetLayer()
            for feature in layer:
                geom = feature.GetGeometryRef()
                xy=np.array([geom.GetX(),geom.GetY()])
                try:
                    XY=np.vstack((XY,xy))
                except:
                    XY=xy
            gtdem= self.ds.GetGeoTransform()
            size=np.array([abs(gtdem[1]),abs(gtdem[5])])
            OS=np.array([gtdem[0],gtdem[3]])
            NumPxl=(np.ceil(abs((XY-OS)/size)-1))#from 0 first cell
            NumPxl[NumPxl==-1.]=0
            driver = self.ds.GetDriver()
            out_data = driver.Create(self.fold + '/inventorynxn.tif', self.xsize, self.ysize, 1, gdal.GDT_Float32)
            #########################
            values=np.zeros((self.ysize,self.xsize),dtype='float32')
            #print 'ciao1'
            if out_data is None:
                QgsMessageLog.logMessage("Could not create output file", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Could not create output file, see 'WoE' Log Messages Panel
            # set values below nodata threshold to nodata
            #print NumPxl
            for i in range(len(NumPxl)):
                #print self.xsize,self.ysize,'xy'
                #print NumPxl[i,1],NumPxl[i,0],'pxl'
                #print max(NumPxl[:,1]),max(NumPxl[:,0]),'max'
                #if NumPxl[i,1]<self.ysize and NumPxl[i,0]<self.xsize:
                if XY[i,1]<self.ymax and XY[i,1]>self.ymin and XY[i,0]<self.xmax and XY[i,0]>self.xmin:
                    values[NumPxl[i,1].astype(int),NumPxl[i,0].astype(int)]=1.
            dem_data = values.astype('float32')
            idxinv=[]
            idxinv=np.where(np.isnan(self.dem))
            values[idxinv]=-9999
            # write the data to output file
            out_band = out_data.GetRasterBand(1)
            out_band.WriteArray(dem_data, 0, 0)
            # flush data to disk, set the NoData value and calculate stats
            out_band.FlushCache()
            #print ds.GetNoDataValue
            out_band.SetNoDataValue(-9999)
            # georeference the image and set the projection
            out_data.SetGeoTransform(self.ds.GetGeoTransform())
            out_data.SetProjection(self.ds.GetProjection())
            #################################dem
        except:
            QgsMessageLog.logMessage("Failure to save sized inventory", tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # Failure to save sized inventory, see 'WoE' Log Messages Panel
        finally:
            del dem_data
            del out_band
            ds9=None
        ###########################################load inventory
        self.catalog=np.array([])
        self.catalog=values
        del values
        #ds4 = gdal.Open(self.fold + '/inventorynxn.tif')
        #if ds4 is None:#######verify if empty
        #    QgsMessageLog.logMessage("ERROR: can't open raster inventory", tag="WoE", level=QgsMessageLog.INFO)
        #    print ERROR
        #self.catalog = np.array(ds4.GetRasterBand(1).ReadAsArray())
        #print self.catalog.dtype
        #print self.catalog
        #print np.min(self.catalog)
        #print 
        #ds4=None
        #######################################clean
        idx4=[]
        idx5=[]
        idx4=np.where(np.isnan(self.catalog))
        self.catalog[idx4]=-9999
        idx5=np.where(np.isnan(self.dem))
        self.dem[idx5]=-9999
        ####################################################verify extension of cause and inventory
        ########dem
        ds6=gdal.Open(self.Wdem)
        ds6x = ds6.RasterXSize
        ds6y = ds6.RasterYSize
        gt5= ds6.GetGeoTransform()
        demxl = round(gt5[0],2)
        demyt = round(gt5[3],2)
        demxr = round(gt5[0] + gt5[1] * ds6x,2)
        demyb = round(gt5[3] + gt5[5] * ds6y,2)
        if self.w < gt5[1] or self.h < gt5[5]:
                    #print('Could not create output file')
                    QgsMessageLog.logMessage('Resolution requested is higher than DEM resolution', tag="WoE", level=QgsMessageLog.INFO)
                    raise ValueError  # Resolution requested is higher than DEM resolution, see 'WoE' Log Messages Panel
        ds6=None
        
        ###########cause
        for v in range(countcause):
            #print v
            ds8=gdal.Open(listcause[v])
            ds8x = ds8.RasterXSize
            ds8y = ds8.RasterYSize
            gt= ds8.GetGeoTransform()
            causexl = round(gt[0],2)
            causeyt = round(gt[3],2)
            causexr = round(gt[0] + gt[1] * ds8x,2)
            causeyb = round(gt[3] + gt[5] * ds8y,2)
            #print gt
            #print gt5
            #print v
            #print causexl,causexr,causeyb,causeyt
            #print self.xmin,self.xmax,self.ymin,self.ymax
            if (causexl)>(self.xmin) or (causexr)<(self.xmax) or (causeyb)>(self.ymin) or (causeyt)<(self.ymax):
                #print('Could not create output file')
                QgsMessageLog.logMessage('Cause %s extension cannot satisfy selected extension'%v, tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Cause extension cannot satisfy selected extension, see 'WoE' Log Messages Panel
            if self.w < gt[1] or self.h < gt[5]:
                        #print('Could not create output file')
                        QgsMessageLog.logMessage('Resolution requested is higher than Cause resolution', tag="WoE", level=QgsMessageLog.INFO)
                        raise ValueError  # Resolution requested is higher than Cause resolution, see 'WoE' Log Messages Panel
            ds8=None
        ###################
        Causes={}
        id={}
        Mat={}
        self.Wfs={}
        for i in range(countcause):
            matrix=None
            self.Wcause=None
            self.classes=None
            self.Wcause=listcause[i]
            self.classes=listclasses[i]
            self.Wreclassed=None
            #self.Weightedcause=None
            #self.txtout=None
            self.Wreclassed=self.fold+'/reclassedcause'+str(i)+'.tif'
            #self.Weightedcause=self.fold+'/weightedcause'+str(i)+'.tif'
            #self.txtout=self.fold+'/Wftxt'+str(i)+'.txt'
            pathszcause=None
            pathszcause=self.fold+'/sizedcause'+str(i)+'.tif'
            if self.polynum==1:
                try:
                    #processing.runalg('saga:clipgridwithpolygon', '/tmp/clipcause'+str(i)+'.tif',self.poly,1,'/tmp/cause'+str(i)+'.tif')
                    os.system('gdalwarp -ot Float32 -q -of GTiff -t_srs EPSG:'+str(self.epsg)+' -r bilinear -overwrite -cutline self.poly -co GEOTIFF_KEYS_FLAVOR=ESRI_PE -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -overwrite -wo OPTIMIZE_SIZE=TRUE '+ self.Wcause+ ' /tmp/clipcause' +str(i)+'.tif')
                except:
                    #print("Failure to set nodata values on raster dem")
                    QgsMessageLog.logMessage("Failure to save clipped input", tag="WoE", level=QgsMessageLog.INFO)
                    raise ValueError  # Failure to save sized input, see 'PhysioClimate' Log Messages Panel
                try:
                    os.system('gdal_translate -of GTiff -ot Float32 -outsize ' + str(self.newXNumPxl) +' '+ str(self.newYNumPxl) +' -projwin ' +str(self.xmin)+' '+str(self.ymax)+' '+ str(self.xmax) + ' ' + str(self.ymin) + ' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 /tmp/clipcause' +str(i)+'.tif /tmp/cause'+str(i)+'.tif')
                except:
                    #print("Failure to set nodata values on raster dem")
                    QgsMessageLog.logMessage("Failure to save sized /tmp input", tag="WoE", level=QgsMessageLog.INFO)
                    raise ValueError  # Failure to save sized input, see 'PhysioClimate' Log Messages Panel
            else:
                try:
                    os.system('gdalwarp -ot Float32 -q -of GTiff -t_srs EPSG:'+str(self.epsg)+' -r bilinear -overwrite '+ self.Wcause+' /tmp/causereproj' +str(i)+'.tif')
                    os.system('gdal_translate -of GTiff -ot Float32 -outsize ' + str(self.newXNumPxl) +' '+ str(self.newYNumPxl) +' -projwin ' +str(self.xmin)+' '+str(self.ymax)+' '+ str(self.xmax) + ' ' + str(self.ymin) + ' -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 ' + '/tmp/causereproj' +str(i)+'.tif /tmp/cause' +str(i)+'.tif')
                except:
                    #print("Failure to set nodata values on raster dem")
                    QgsMessageLog.logMessage("Failure to save sized input", tag="WoE", level=QgsMessageLog.INFO)
                    raise ValueError  # Failure to save sized input, see 'PhysioClimate' Log Messages Panel
            self.matrix=None
            self.RasterInt=None
            ###################
            ds2=None
            #print ds2
            ds2 = gdal.Open('/tmp/cause' +str(i)+'.tif')
            if ds2 is None:#####################verify empty row input
                QgsMessageLog.logMessage("ERROR: can't open raster input", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # can't open raster input, see 'WoE' Log Messages Panel
            a=ds2.GetRasterBand(1)
            NoData=a.GetNoDataValue()
            #print NoData
            self.RasterInt = np.array(a.ReadAsArray()).astype('int16')
            self.matrix = np.array(a.ReadAsArray()).astype('float32')
            bands = ds2.RasterCount
            if bands>1:#####################verify bands
                QgsMessageLog.logMessage("ERROR: input rasters shoud be 1-band raster", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # input rasters shoud be 1-band raster, see 'WoE' Log Messages Panel
            #########################################nodata -9999, clean nan values
            idx6=[]
            idx7=[]
            idx6=np.where(np.isnan(self.matrix))
            self.matrix[idx6]=-9999
            self.RasterInt=self.matrix.astype('int64')
            idx7=np.where(np.isnan(self.RasterInt))
            self.RasterInt[idx7]=-9999
            self.matrix[self.matrix==NoData]= -9999
            self.RasterInt[self.matrix==NoData]= -9999
            ###########################################
            driverC = self.ds.GetDriver()
            out_dataC = driverC.Create(pathszcause, self.xsize, self.ysize, 1, gdal.GDT_Float32)
            dataC = self.matrix.astype('float32')
            out_bandC = out_dataC.GetRasterBand(1)
            out_bandC.WriteArray(dataC, 0, 0)
            out_bandC.FlushCache()
            out_bandC.SetNoDataValue(-9999)
            out_dataC.SetGeoTransform(self.ds.GetGeoTransform())
            out_dataC.SetProjection(self.ds.GetProjection())
            ################################################################
            self.classification()#############
            self.save()####################
            Causes[i]=self.RasterInt
            Mat[i]=self.matrix
            id[i]=np.where(self.RasterInt==-9999)
            ##################################-9999
            self.matrix=None
            self.RasterInt=None
            out_bandC=None
            dataC=None
        self.Raster=np.array([])
        self.Matrix=np.array([])
        for causa in range(countcause):
            self.txtout=None
            self.Weightedcause=None
            self.txtout=self.fold+'/Wftxt'+str(causa)+'.txt'
            self.Weightedcause=self.fold+'/weightedcause'+str(causa)+'.tif'
            self.Raster=Causes[causa]
            self.Matrix=Mat[causa]
            for cc in range(countcause):
                self.Raster[id[cc]]=-9999
                self.Matrix[id[cc]]=-9999
            self.WoE()#################
            self.Wfs[causa]=self.weighted
            self.saveWf()##################
            self.weighted=np.array([])
        #self.dem=np.array([])
        #self.catalog=np.array([])
        #Causes={}
        #Mat={}
        #id={}
        #self.Matrix=np.array([])
        #self.Raster=np.array([])
        del self.dem
        del self.catalog
        del Causes
        del Mat
        del id
        del self.Matrix
        del self.Raster


    def classification(self):###############classify causes according to txt classes
        Min={}
        Max={}
        clas={}
        with open(self.classes, 'r') as f:
            c = csv.reader(f,delimiter=' ')
            count=1
            for cond in c:
                #print cond
                b=np.array([])
                b=np.asarray(cond)
                #print b[0]
                Min[count]=b[0].astype(int)
                Max[count]=b[1].astype(int)
                clas[count]=b[2].astype(int)
                count+=1
        key_max=None
        key_min=None
        key_max = max(Max.keys(), key=(lambda k: Max[k]))
        key_min = min(Min.keys(), key=(lambda k: Min[k]))
        idx=np.where(np.isnan(self.RasterInt))
        self.matrix[idx]=-9999
        self.RasterInt[idx]=-9999
        self.matrix[(self.RasterInt<Min[key_min])]=-9999
        self.RasterInt[(self.RasterInt<Min[key_min])]=-9999
        self.matrix[(self.RasterInt>Max[key_max])]=-9999
        self.RasterInt[(self.RasterInt>Max[key_max])]=-9999
        for i in range(1,count):
            self.matrix[(self.RasterInt>=Min[i])&(self.RasterInt<=Max[i])]=clas[i]
            self.RasterInt[(self.RasterInt>=Min[i])&(self.RasterInt<=Max[i])]=clas[i].astype(int)
        #print Max[key_max],Min[key_min]
        #print self.matrix[(self.RasterInt<Min[key_min])]
        #print self.RasterInt[self.RasterInt==1]
            #print clas[i]#################àse una classe non viene consideraa allora vale nulla

    def save(self):#############save causes reclassed
        try:
            out_data = None
            # read in data from first band of input raster
            cols = self.xsize
            rows = self.ysize
            # create single-band float32 output raster
            driver = self.ds.GetDriver()
            out_data = driver.Create(self.Wreclassed, cols, rows, 1, gdal.GDT_Float32)
            if out_data is None:
                QgsMessageLog.logMessage("Could not create output file", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Could not create output file, see 'WoE' Log Messages Panel
            # set values below nodata threshold to nodata
            dem_data=np.array([])
            dem_data = self.RasterInt
            # write the data to output file
            out_band = out_data.GetRasterBand(1)
            out_band.WriteArray(dem_data, 0, 0)
            # flush data to disk, set the NoData value and calculate stats
            out_band.FlushCache()
            out_band.SetNoDataValue(-9999)
            # georeference the image and set the projection
            out_data.SetGeoTransform(self.ds.GetGeoTransform())
            out_data.SetProjection(self.ds.GetProjection())
        except:
            QgsMessageLog.logMessage("ERROR: failure to save reclassed cause", tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # failure to save reclassed cause, see 'WoE' Log Messages Panel
        finally:
            del dem_data
            del out_band

    def WoE(self):######################calculate W+,W-,Wf
        ################################################
        idx=[]
        idx1=[]
        idx2=[]
        idx3=[]
        idx=np.where(np.isnan(self.catalog))
        self.catalog[idx]=-9999
        idx1=np.where(np.isnan(self.dem))
        self.dem[idx1]=-9999
        ###############################################
        product=np.array([])
        diff=np.array([])
        product=(self.catalog*self.Raster)
        diff=(self.Raster-product)
        ######################################clean nan values
        idx2=np.where(self.catalog==-9999)
        product[idx2]=-9999
        diff[idx2]=-9999
        idx3=np.where(self.dem==-9999)
        product[idx3]=-9999
        diff[idx3]=-9999
        product[self.Raster==-9999]=-9999
        diff[self.Raster==-9999]=-9999
        ############################################
        M=int(np.nanmax(self.Raster))
        countProduct = {}
        countDiff = {}
        for n in range(0,M+1):
            P=np.array([])
            D=np.array([])
            P=np.argwhere(product==float(n))
            PP=float(len(P))
            countProduct[n]=PP
            D=np.argwhere(diff==n)
            DD=float(len(D))
            countDiff[n]=DD
        self.weighted=np.array([])
        self.weighted=self.Matrix
        file = open(self.txtout,'w')#################save W+, W- and Wf
        for i in range(1,M+1):
            #print i
            Npx1=None
            Npx2=None
            Npx3=None
            Npx4=None
            Wplus=None
            Wminus=None
            Wf=None
            var=[]
            #print countProduct[i],countDiff[i]
            if countProduct[i]==0 or countDiff[i]==0:
                Wf=0.
                Wplus=0.
                Wminus=0.
                var=[i,Wplus,Wminus,Wf]
                file.write('class,W+,W-,Wf: %s\n' %var)
                self.weighted[self.Raster == i] = 0.
            else:
                Npx1=float(countProduct[i])
                for ii in range(1,M+1):
                    try:
                        Npx2 += float(countProduct[ii])
                    except:
                        Npx2 = float(countProduct[ii])
                Npx2 -= float(countProduct[i])
                Npx3=float(countDiff[i])
                for iii in range(1,M+1):
                    try:
                        Npx4 += float(countDiff[iii])
                    except:
                        Npx4 = float(countDiff[iii])
                Npx4 -= float(countDiff[i])
                #W+ W-
                #print Npx1,Npx2,Npx3,Npx4
                if Npx1==0 or Npx3==0:
                    Wplus=0.
                else:
                    Wplus=math.log((Npx1/(Npx1+Npx2))/(Npx3/(Npx3+Npx4)))
                if Npx2==0 or Npx4==0:
                    Wminus=0.
                else:
                    Wminus=math.log((Npx2/(Npx1+Npx2))/(Npx4/(Npx3+Npx4)))
                Wf=Wplus-Wminus
                var=[i,Wplus,Wminus,Wf]
                file.write('class,W+,W-,Wf: %s\n' %var)#################save W+, W- and Wf
                self.weighted[self.Raster == i] = Wf
        file.close()
        product=np.array([])
        diff=np.array([])

    def saveWf(self):
        try:
            out_data = None
            # read in data from first band of input raster
            cols = self.xsize
            rows = self.ysize
            # create single-band float32 output raster
            driver = self.ds.GetDriver()
            out_data = driver.Create(self.Weightedcause, cols, rows, 1, gdal.GDT_Float32)
            if out_data is None:
                #print('Could not create output file')
                QgsMessageLog.logMessage("Could not create output file", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Could not create output file, see 'WoE' Log Messages Panel
            # set values below nodata threshold to nodata
            dem_data=np.array([])
            dem_data = self.weighted
            # write the data to output file
            out_band = out_data.GetRasterBand(1)
            out_band.WriteArray(dem_data, 0, 0)
            # flush data to disk, set the NoData value and calculate stats
            out_band.FlushCache()
            out_band.SetNoDataValue(-9999)
            # georeference the image and set the projection
            out_data.SetGeoTransform(self.ds.GetGeoTransform())
            out_data.SetProjection(self.ds.GetProjection())
        except:
            #print("Failure to set nodata values on raster Wf")
            QgsMessageLog.logMessage("Failure to set nodata values on raster Wf", tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # Failure to set nodata values on raster Wf, see 'WoE' Log Messages Panel
            #print ERROR
        finally:
            del dem_data
            del out_band

    def sumWf(self):
        self.LSI=sum(self.Wfs.values())
        for iii in self.Wfs:
            idx=[]
            idx9=[]
            self.Wfs[iii]=self.Wfs[iii].astype(int)
            idx=np.where(np.isnan(self.Wfs[iii]))
            self.LSI[idx]=-9999
            self.LSI[self.LSI==-9999]=-9999
            idx9=np.where(self.Wfs[iii]==-9999)
            self.LSI[idx9]=-9999
        del self.Wfs

    def saveLSI(self):
        try:
            out_data = None
            # read in data from first band of input raster
            cols = self.xsize
            rows = self.ysize
            # create single-band float32 output raster
            driver = self.ds.GetDriver()
            out_data = driver.Create(self.LSIout, cols, rows, 1, gdal.GDT_Float32)
            if out_data is None:
                QgsMessageLog.logMessage("ERROR: Could not create output file", tag="WoE", level=QgsMessageLog.INFO)
                raise ValueError  # Could not create output file, see 'WoE' Log Messages Panel
                #print ERROR
            # set values below nodata threshold to nodata
            dem_data = self.LSI
            # write the data to output file
            out_band = out_data.GetRasterBand(1)
            out_band.WriteArray(dem_data, 0, 0)
            # flush data to disk, set the NoData value and calculate stats
            out_band.FlushCache()
            out_band.SetNoDataValue(-9999)
            # georeference the image and set the projection
            out_data.SetGeoTransform(self.ds.GetGeoTransform())
            out_data.SetProjection(self.ds.GetProjection())
        except:
            #print("Failure to set nodata values on raster LSI")
            QgsMessageLog.logMessage("ERROR: Failure to set nodata values on raster LSI", tag="WoE", level=QgsMessageLog.INFO)
            raise ValueError  # Failure to set nodata values on raster LSI, see 'WoE' Log Messages Panel
            #print ERROR
        finally:
            del out_data
            del dem_data
            del self.LSI

