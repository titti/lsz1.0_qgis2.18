import numpy as np
from osgeo import gdal
import sys
import math
import csv
import sys
sys.path.append('/home/irpi/PluginsAndModels/WoE')
from classe import model
test = model()

test.Wcause1='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/SRTM_NE_250m_3857.tif'
test.classes1='/home/irpi/SinoItalian_Lab/Tier1_3857/Classes/DEM.txt'
test.Wcause2='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/SlopeNE250m_3857.tif'
test.classes2='/home/irpi/SinoItalian_Lab/Tier1_3857/Classes/Slope.txt'
test.Wcause3='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/GLOBCOVER_3857.tif'
test.classes3='/home/irpi/SinoItalian_Lab/Tier1_3857/Classes/LandCover.txt'
test.Wcause4='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/USGS_Geology_EurAsia_1km_3857.tif'
test.classes4='/home/irpi/SinoItalian_Lab/Tier1_3857/Classes/Lithology.txt'
#test.inventory='/home/irpi/SinoItalian_Lab/data_prova/test7_2040N6080E/SizedCauses/inventory0-10km.tif'
test.inventory='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/nasa_1km-yangze3857.shp'
test.Wdem='/home/irpi/SinoItalian_Lab/Tier1_3857/Data/SRTM_NE_250m_3857.tif'
test.LSIout='/home/irpi/SinoItalian_Lab/Tier1_3857/OutputWoE/LSI.tif'
test.fold='/home/irpi/SinoItalian_Lab/Tier1_3857/OutputWoE'
test.poly=''
test.polynum=0
#xmin,ymin,xmax,ymax
test.xmax=4300000
test.ymax=4300000
test.xmin=2500000
test.ymin=2500000
test.w=1000
test.h=1000


test.iter()
test.sumWf()
test.saveLSI()
del test